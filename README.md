### Сборка контейнера

```
docker build -t hello-world-app .
```

### Публикация в Docker Hub

```
docker login
docker tag hello-world-app your_dockerhub_nickname/hello-world-app
docker push your_dockerhub_nickname/hello-world-app 
```

![Alt text](result_images/image-3.png)

### Сборка и тестирование

В Gitlab переходим в раздел: Settings -> CI/CD -> Variables

Создаём переменные среды (Add variable)

![Alt text](result_images/image.png)
```
CI_REGISTRY_USER : your_dockerhub_nickname
CI_REGISTRY_PASSWORD : your_dockerhub_password
CI_REGISTRY_IMAGE : docker.io/your_dockerhub_nickname/hello-world-app
```

**Результат:**

![Alt text](result_images/image-4.png)

![Alt text](result_images/image-5.png)